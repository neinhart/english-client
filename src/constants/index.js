import { DUMMY_DATA } from './data';

export const ENV_DEV = process.env.NODE_ENV !== `production`;

export const KEYCODES = {
  BACK: 4,
  DPAD_UP: 19,
  DPAD_DOWN: 20,
  DPAD_LEFT: 21,
  DPAD_RIGHT: 22,
  DPAD_CENTER: 23,
  MENU: 82,
};

export * from './data';