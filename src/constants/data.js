export const DUMMY_DATA = {
  contents: [
    {
      "id": "11",
      "name": "Gaspar Brasserie",
      "address": "185 Sutter St, San Francisco, CA 94109",
      "image": { "url": "https://shoutem.github.io/static/getting-started/restaurant-1.jpg" },
    },
    {
      "id": "12",
      "name": "Chalk Point Kitchen",
      "address": "527 Broome St, New York, NY 10013",
      "image": { "url": "https://shoutem.github.io/static/getting-started/restaurant-2.jpg" },
    },
    {
      "id": "13",
      "name": "Kyoto Amber Upper East",
      "address": "225 Mulberry St, New York, NY 10012",
      "image": { "url": "https://shoutem.github.io/static/getting-started/restaurant-3.jpg" },
    },
    {
      "id": "14",
      "name": "Gaspar Brasserie",
      "address": "185 Sutter St, San Francisco, CA 94109",
      "image": { "url": "https://shoutem.github.io/static/getting-started/restaurant-1.jpg" },
    },
    {
      "id": "15",
      "name": "Chalk Point Kitchen",
      "address": "527 Broome St, New York, NY 10013",
      "image": { "url": "https://shoutem.github.io/static/getting-started/restaurant-2.jpg" },
    },
    {
      "id": "16",
      "name": "Kyoto Amber Upper East",
      "address": "225 Mulberry St, New York, NY 10012",
      "image": { "url": "https://shoutem.github.io/static/getting-started/restaurant-3.jpg" },
    }
  ],
  contents2: [
    {
      id: 1,
      title: "List Title1",
      data: [
        {
          "id": "11",
          "name": "Gaspar Brasserie",
          "address": "185 Sutter St, San Francisco, CA 94109",
          "image": { "url": "https://shoutem.github.io/static/getting-started/restaurant-1.jpg" },
        },
        {
          "id": "12",
          "name": "Chalk Point Kitchen",
          "address": "527 Broome St, New York, NY 10013",
          "image": { "url": "https://shoutem.github.io/static/getting-started/restaurant-2.jpg" },
        },
        {
          "id": "13",
          "name": "Kyoto Amber Upper East",
          "address": "225 Mulberry St, New York, NY 10012",
          "image": { "url": "https://shoutem.github.io/static/getting-started/restaurant-3.jpg" },
        },
        {
          "id": "14",
          "name": "Gaspar Brasserie",
          "address": "185 Sutter St, San Francisco, CA 94109",
          "image": { "url": "https://shoutem.github.io/static/getting-started/restaurant-1.jpg" },
        },
        {
          "id": "15",
          "name": "Chalk Point Kitchen",
          "address": "527 Broome St, New York, NY 10013",
          "image": { "url": "https://shoutem.github.io/static/getting-started/restaurant-2.jpg" },
        },
        {
          "id": "16",
          "name": "Kyoto Amber Upper East",
          "address": "225 Mulberry St, New York, NY 10012",
          "image": { "url": "https://shoutem.github.io/static/getting-started/restaurant-3.jpg" },
        }
      ]
    },
    {
      id: 2,
      title: "List Title2",
      data: [
        {
          "id": "21",
          "name": "Gaspar Brasserie",
          "address": "185 Sutter St, San Francisco, CA 94109",
          "image": { "url": "https://shoutem.github.io/static/getting-started/restaurant-1.jpg" },
        },
        {
          "id": "22",
          "name": "Chalk Point Kitchen",
          "address": "527 Broome St, New York, NY 10013",
          "image": { "url": "https://shoutem.github.io/static/getting-started/restaurant-2.jpg" },
        },
        {
          "id": "23",
          "name": "Kyoto Amber Upper East",
          "address": "225 Mulberry St, New York, NY 10012",
          "image": { "url": "https://shoutem.github.io/static/getting-started/restaurant-3.jpg" },
        },
        {
          "id": "24",
          "name": "Gaspar Brasserie",
          "address": "185 Sutter St, San Francisco, CA 94109",
          "image": { "url": "https://shoutem.github.io/static/getting-started/restaurant-1.jpg" },
        },
        {
          "id": "25",
          "name": "Chalk Point Kitchen",
          "address": "527 Broome St, New York, NY 10013",
          "image": { "url": "https://shoutem.github.io/static/getting-started/restaurant-2.jpg" },
        },
        {
          "id": "26",
          "name": "Kyoto Amber Upper East",
          "address": "225 Mulberry St, New York, NY 10012",
          "image": { "url": "https://shoutem.github.io/static/getting-started/restaurant-3.jpg" },
        }
      ]
    }
  ]
};