import ContentStore from './Contents';

class RootStore {
  constructor() {
    this.contentStore = new ContentStore(this);
  }
}

export default RootStore;