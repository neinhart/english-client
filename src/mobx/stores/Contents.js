import { observable, action, computed, toJS, autorun } from 'mobx';

import { DUMMY_DATA } from '../../constants';

export default class ContentStore {
  @observable categoryList = []
  
  constructor(root) {
    this.root = root;

    this.categoryList = DUMMY_DATA.contents2;
  }

  @computed get cateList() {
    return toJS(this.categoryList);
  }
}