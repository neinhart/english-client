import React, {Component} from 'react';
import { StyleSheet, View } from 'react-native';
//import { Container } from 'native-base';

import { Provider } from 'mobx-react';

import { ENV_DEV } from './constants';
import DevTools from 'mobx-react-devtools';

import ContentList from './components/category';

import RootStore from './mobx/stores';

const root = new RootStore();

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {

    console.log('render', root)

    return (
      <Provider {...root}>
        <View style={styles.container}>
          <ContentList />

          {/* ENV_DEV && <DevTools /> */}
        </View>
      </Provider>
      
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000'
  }
});

export default App;