import React, {Component} from 'react';
import _ from 'lodash';

// Libraries
import { observer, inject } from 'mobx-react';
import KeyEvent from 'react-native-keyevent';

// Constants
import { KEYCODES } from '../../constants';

var contain = (Present)  => {
  @inject(({ ...root }) => ({ 
    categoryList: root.contentStore.cateList
  }))
  @observer
  class Container extends Component {
    // PROPS SETTING
    static propTypes = {
    }

    static defaultProps = {
    }

    // CLASS INNER FUNCTIONS
    constructor(props) {
      super(props);
      this.state = {
        focus: {
          section: 0,
          index: 0
        }
      }

      // bind to dispatch function(s)
      this._onPressItem = this._onPressItem.bind(this);
      this._setListRef = this._setListRef.bind(this);
      this._setSectionListRef = this._setSectionListRef.bind(this);

      this.listRef = new Map();
      this.sectionListRef = null;
    }

    render() {
      let presentProps = [ 'categoryList' ];
      let presentState = [ 'focus' ];
      let customProps = {};
      let presentFunctions = {
        onPressItem: this._onPressItem,
        setListRef: this._setListRef,
        setSectionListRef: this._setSectionListRef
      }

      return (  // Do not modify!!
        <Present
          props={{...(_.pick(this.props, presentProps)), ...customProps}}
          state={_.pick(this.state, presentState)}
          func={presentFunctions}
        />
      )
    }

    // COMPONENT LIFE CYCLE
    componentWillMount() {}
    componentWillReceiveProps(nextProps) {}
    //shouldComponentUpdate(nextProps, nextState) { return true }
    componentWillUpdate(nextProps, nextState) {}
    componentDidUpdate() {}

    // TODO 이곳에서만 key event를 받으면 각 component 마다 KeyListener를 구현해야 한다.
    // 그러므로 KeyListener Manager를 이용해서 component 는 HoC로 keyListening 하는 구조로 변경이 필요하다.
    componentDidMount() {
      KeyEvent.onKeyDownListener((keyEvent) => {
        //console.log(`onKeyDown keyCode: ${keyEvent.keyCode}`);
        //console.log(`Action: ${keyEvent.action}`);
      });
   
      // if you want to react to keyUp
      KeyEvent.onKeyUpListener((keyEvent) => {
        console.log(`onKeyUp keyCode: ${keyEvent.keyCode}`);
        console.log(`Action: ${keyEvent.action}`);

        this._handleKeyEvent(keyEvent);
      });
   
      // if you want to react to keyMultiple
      /*KeyEvent.onKeyMultipleListener((keyEvent) => {
        console.log(`onKeyMultiple keyCode: ${keyEvent.keyCode}`);
        console.log(`Action: ${keyEvent.action}`);
        console.log(`Characters: ${keyEvent.characters}`);
      });*/
    }

    componentWillUnmount() {
      KeyEvent.removeKeyDownListener();
      // if you are listening to keyUp
      KeyEvent.removeKeyUpListener();
      // if you are listening to keyMultiple
      //KeyEvent.removeKeyMultipleListener();

    }

    // CUSTOM PROPS FUNCTIONS
    _setListRef = (id, ref) => {
      if (!this.listRef.has(id)) {
        this.listRef.set(id, ref);
        console.log(this.listRef);
      }
    }

    _setSectionListRef = (ref) => {
      if (!this.sectionListRef) {
        this.sectionListRef = ref;
      }
    }

    _onPressItem = (index) => {
      this.setState({
        focusIndex: index
      });
    };

    // CUSTOM INNER FUNCTIONS
    _handleKeyEvent = (evt) => {
      const { categoryList } = this.props;
      const { focus } = this.state;

      let scroll = true;
      let newFocus = { section: focus.section, index: focus.index };
      const sectionLength = categoryList.length;
      const listLength = categoryList[focus.section].data.length;
      switch (evt.keyCode) {
        case KEYCODES.DPAD_RIGHT:
            if (focus.index < listLength - 1) {
              ++newFocus.index;
              
            }
          break;
        case KEYCODES.DPAD_LEFT:
          if (focus.index > 0) {
            --newFocus.index;
            this._changeFocus(focus, newFocus);
          }
          break;
        case KEYCODES.DPAD_UP:
          if (focus.section > 0) {
            --newFocus.section;
            newFocus.index = 0;
            scroll = false;
          }
          break;
        case KEYCODES.DPAD_DOWN:
          if (focus.section < sectionLength - 1) {
            ++newFocus.section;
            newFocus.index = 0;
            scroll = false;
          }
          break;
      }

      this._changeFocus(focus, newFocus);
    }

    _changeFocus = (focus, newFocus, scroll = true) => {
      //console.log('changeFocus', focus, newFocus)
      if (focus.section === newFocus.section && focus.index === newFocus.index) {
        return;
      }

      // first, scroll to focus item
      this._scrollTo(focus, newFocus);

      this.setState({
        focus: newFocus
      });
    }

    _scrollTo = (curFocus, focus) => {
      const curListRef = this.listRef.get(curFocus.section + 1);
      if (curListRef) {
        curListRef.scrollToOffset({animated: false, offset: 0});
      }

      const listRef = this.listRef.get(focus.section + 1);
      if (listRef) {
        //console.log(listRef, focus);
        //const index = focus.index === 0 ? 0 : focus.index - 1;
        const pos = focus.index === 0 ? 1 : 0.5;
        listRef.scrollToIndex({animated: true, index: focus.index, viewPosition: pos});
      }

      // up, down 시에 SectionList의 scroll focus 위치를 조정
      if (this.sectionListRef) {
        this.sectionListRef.scrollToLocation({
          animated: true, 
          sectionIndex: focus.section,
          itemIndex: focus.index,
          viewPosition: 1
        });
      }
    }
  }

  return Container;
}

export default contain