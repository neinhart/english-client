import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    height: 300
  },
  image: {
    width: 260,
    height: 140
  },
  focusedImage: {
    width: 320,
    height: 210
  },
  dimmed: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.6)'
  },
  focusedDimmed: {
    backgroundColor: 'transparent'
  },
  normalText: {
    color: 'rgba(255, 255, 255, 0.3)',
    fontSize: 10,
  },
  focusedNormalText: {
    color: '#fff',
    fontSize: 10,
  },
  boldText: {
    fontSize: 14,
    color: 'rgba(255, 255, 255, 0.3)',
    fontWeight: 'bold'
  },
  focusedBoldText: {
    fontSize: 14,
    color: '#fff',
    fontWeight: 'bold'
  }
})