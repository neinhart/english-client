import React, { Component } from "react";
import contain from './Container';

import { FlatList, SectionList, View, Text} from "react-native";

import ContentListItem from './content/ContentListItem';

import { styles } from './style';

const Present = ({ ...prop }) => {
  const { categoryList } = prop.props;
  const { focus } = prop.state;
  const { onPressItem, setListRef, setSectionListRef } = prop.func;

  console.log('focus', focus);

  return (
    <SectionList
      ref={(ref) => setSectionListRef(ref)}
      renderSectionHeader={({section: {title, data, id}}) => {
        //console.log(id, title)

        return (
          <View style={styles.container}>
            <Text style={styles.header}>{title}</Text>

            <FlatList
              ref={(ref) => setListRef(id, ref)}
              data={data}
              horizontal
              extraData={{focus}}
              ItemSeparatorComponent={() => (
                <View style={{width: 30, height: '100%', backgroundColor: 'transparent'}}/>
              )}
              renderItem={({ item: rowData, index }) => {
                //console.log('renderItem', rowData)
                return (
                  <ContentListItem
                    item={rowData}
                    onPressItem={onPressItem}
                    focused={id - 1 === focus.section && index === focus.index}
                  />
              )}}
              keyExtractor={(item, index) => item.id}
              directionalLockEnabled={true}
            />
          </View>
        )
      }}
      renderItem={({ item, index, section }) => {
        return null
      }}
      sections={categoryList}
      keyExtractor={(item, index) => item.id}
    />
  )

};


export default contain(Present)