import React, { Component } from "react";

import { View, Text, TouchableWithoutFeedback, ImageBackground, TouchableHighlight } from "react-native";
import { ListItem } from 'react-native-elements';

// Styles
import { styles } from '../style/ContentListItem';

class ContentListItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      focused: props.focused
    }
  }

  componentWillReceiveProps(props) {
    this.setState({
      focused: props.focused
    });
  }

  _onPress = () => {
    this.props.onPressItem(this.props.item.id);
  };

  render() {
    const { item } = this.props;
    const { focused } = this.state;

    //console.log('ContentListItem', focused)

    let imageStyles = [ styles.image ];
    let dimmedStyles = [ styles.dimmed ];
    let textStyle = [ styles.boldText ];
    let subTextStyle = [ styles.normalText ];
    if (focused) {
      imageStyles.push(styles.focusedImage);
      dimmedStyles.push(styles.focusedDimmed);
      textStyle.push(styles.focusedBoldText);
      subTextStyle.push(styles.focusedNormalText);
    }

    return (
      <TouchableHighlight
        style={styles.root}
        onFocus={() => {
          console.log('ContentListItem', 'onFocus');
        }}
        onBlur={() => {
          console.log('ContentListItem', 'onBlur');
        }}
      >
        <ListItem
          containerStyle={styles.container}
          subtitle={
            <View>
              <ImageBackground 
                source={{ uri: item.image.url }}
                style={imageStyles}
              >
                <View style={dimmedStyles}></View>
              </ImageBackground>

              <Text style={textStyle}>{item.name}</Text>
              <Text style={subTextStyle}>{item.address}</Text>
            </View>
          }
        />
      </TouchableHighlight>
    );
  }
}

export default ContentListItem;