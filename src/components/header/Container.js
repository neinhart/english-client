import React, {Component} from 'react';
import _ from 'lodash';

var contain = (Present)  => {
  class Container extends Component {
    // PROPS SETTING
    static propTypes = {
    }

    static defaultProps = {
    }

    // CLASS INNER FUNCTIONS
    constructor(props) {
      super(props);
      this.state = {
      }

      // bind to dispatch function(s)
      this._refresh = this._refresh.bind(this);
    }

    render() {
      let presentProps = [];
      let presentState = [];
      let customProps = {};
      let presentFunctions = {
        refresh: this._refresh
      }

      return (  // Do not modify!!
        <Present
          props={{...(_.pick(this.props, presentProps)), ...customProps}}
          state={_.pick(this.state, presentState)}
          func={presentFunctions}
        />
      )
    }

    // COMPONENT LIFE CYCLE
    componentWillMount() {
    }

    componentWillReceiveProps(nextProps) {
    }

    //shouldComponentUpdate(nextProps, nextState) { return true }

    componentWillUpdate(nextProps, nextState) {
    }

    componentDidMount() {}
    componentDidUpdate() {}
    componentWillUnmount() {}

    // CUSTOM FUNCTIONS
    _refresh = () => {
    }
  }

  return Container;
}

export default contain