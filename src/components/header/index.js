import React from 'react';

import contain from './Container';

import { Container, Header, Body, Title, Item, Icon, Input, Button, Text } from 'native-base';

import { styles } from './style';

const Present = ({ ...prop }) => {
  const {} = prop.props;
  const {} = prop.state;
  const {} = prop.func;

  return (
    <Container>
      <Header noShadow searchBar>
        <Body>
          <Title>EDU Client ABC</Title>
        </Body>
        <Item>
          <Icon name="ios-search" />
          <Input placeholder="Search" />
          <Icon name="ios-people" />
        </Item>
        <Button transparent>
          <Text>Search</Text>
        </Button>
      </Header>
    </Container>
  );
};

export default contain(Present)