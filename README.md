# english-client #

react-native 기반의 english client application 

## 개발 환경 설정 ###

* git clone 및 npm install
```
$ git clone git@bitbucket.org:alti_ai/english-client.git

$ npm install
```
* react native 설치
```
$ npm i -g react-native-cli
```

## Android TV 환경 설정 ##
*기본적으로 android studio를 통해 SDK 설치는 되어 있다고 전제한다.*

#### Emulator ####
Android Studio 메뉴에서 tools/AVD manager 선택
AVD manager dialog에서 하단의 "Create Virtual Device" 버튼을 눌러 테스트 하고자 하는 android TV device 추가

* android SDK path 지정
** home 폴더의 .bashrc 에 path 추가
```
ANDROID_HOME='/home/{userId}/Android/Sdk'
export PATH=$PATH:$ANDROID_HOME/emulator:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools
```

* AVD 실행
```
# AVD 리스트를 확인하고 실행하고자 하는 AVD를 선택하여 emulator로 실행 (-verbose 옵션은 디버깅 목적임)

$ emulator -list-avds
Android_TV_1080p_API_26

$ emulator @Android_TV_1080p_API_26 -verbose
```

* troubleshoot
** AVD 실행 시 libGL 및 driver 오류 발생 시
```
#방안 1
$ emulator -use-system-libs @Android_TV_1080p_API_26

#방안 2
$ sudo apt-get install lib64stdc++6:i386
$ sudo apt-get install mesa-utils (아마 기존에 설치되어 있을 것이다)
      
$ mv ~/Android/Sdk/emulator/lib64/libstdc++/libstdc++.so.6{,.bak}
$ mv ~/Android/Sdk/emulator/lib64/libstdc++/libstdc++.so.6.0.18{,.bak}
$ ~/Android/Sdk/emulator/lib64/libstdc++$ ln -s /usr/lib32/libstdc++.so.6 ./
```
** driver 오류는 발생하지 않지만 AVD가 정상적으로 실행되지 않는 경우
*** 이 때에는 graphic card 에서 GPU를 사용하지 않는 옵션을 적용
```
$ emulator @Android_TV_1080p_API_26 -verbose -gpu off
```


## App 실행 ##
*다음의 순서대로 실행한다.*

* emulator 실행
** package.json 의 emul script 에서 자신의 emul AVD로 변경하고 실행해야 함
```
$ npm run emul
```
* react native start
```
$ npm run native:reset
```
* app 빌드 및 run
```
$ npm run android
```